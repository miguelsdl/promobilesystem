from django.db import models

from wagtail.contrib.modeladmin.options import ModelAdmin

from promobile.lib.helpers.field_definition import FieldDefinition


class OutMessages(models.Model):

    oper = FieldDefinition.oper
    command = FieldDefinition.command
    msisdn = FieldDefinition.msisdn
    message = FieldDefinition.message

    ready = models.BooleanField(verbose_name=u'Listo para envia', default=True)
    sending = models.BooleanField(verbose_name=u'En proceso', default=False)
    sent = models.BooleanField(verbose_name=u'Enviado', default=False)
    delivered = models.BooleanField(verbose_name=u'Entregado', default=False)
    billed = models.BooleanField(verbose_name=u'Cobrado', default=False)


class Admin(ModelAdmin):
    model = OutMessages
    menu_label = "Mensajes salientes"
    list_display = ('oper', 'command', "msisdn", "message", "ready", "sending", "sent", "delivered", "billed")
    list_filter = ('oper', 'command', "msisdn", "message", "ready", "sending", "sent", "delivered", "billed")
    ordening = ('oper', 'command', "msisdn", "message", "ready", "sending", "sent", "delivered", "billed")
    search_fields = ('oper', 'command', "msisdn", "message", "ready", "sending", "sent", "delivered", "billed")


