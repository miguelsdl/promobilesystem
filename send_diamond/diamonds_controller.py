import logging
from datetime import timedelta

from background_task import background
from django.conf import settings

from inapp.lib.types.request import Request
from voting.models.commands import Commands
from voting.models.voting_messages import VotingMessages
from send_diamond.models.out_messages import OutMessages

from promobile.lib.helpers.helpers import messages_manager

l = logging.getLogger(__name__)

'''
Es el punto de entrada para el envío de messages, ejecuta cada tarera en un hilo aparte
'''


class DiamondsController:

    @staticmethod
    @background(schedule=3)
    def send(**kwargs):
        # def send(oper=None, message_id=None, command_id=None, request=None):
        """
        Manda a ejecutar en un hilo aparte la tarea de enviar message según la operadora
        :param oper: operadora
        :param message: message
        :param command: command
        :return: Bool
        """

        try:
            message_id = kwargs.get("message_id")
            request = kwargs.get("request")
            out_message_id = kwargs.get("out_message_id")
            message_manager_name = kwargs.get("message_manager_name")

            message = messages_manager.get(message_manager_name).objects.get(pk=message_id)
            oper = request.get("oper")

            if oper == 1:
                l.info("DiamondsController::send() Antel")
                from send_diamond.diamonds.antel import Antel as Sender
                res = Sender.send_ondemand(message, request)

            if oper == 2:
                l.info("DiamondsController::send() Movistar")
                from send_diamond.diamonds.movistar import Movistar as Sender
                res = Sender.send_ondemand(message, request)

            if oper == 3:
                l.info("DiamondsController::send() Claro")
                from send_diamond.diamonds.claro import Claro as Sender
                res = Sender.send_ondemand(message, request)

            o = OutMessages.objects.get(pk=out_message_id)
            if res:
                o.sent = True
                o.ready = False

            o.save()

        except Exception as all:
            raise all


