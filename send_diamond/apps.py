from django.apps import AppConfig


class SendDiamondConfig(AppConfig):
    name = 'send_diamond'
