import requests
import logging

from django.conf import settings

from inapp.lib.types.request import Request as RequestType
l = logging.getLogger(__name__)


class Antel:

    @staticmethod
    def send_ondemand(message, request: RequestType):
        l.info("Antel::send_ondemand() SENT")
        url = settings.URL_SEND_ONDEMAND_ANTEL.format(
            msisdn=request.get("msisdn"),
            message=message.message,
            short_code=request.get("short_code")
        )
        try:
            result = requests.get(url=url)
            l.info("Antel::send_ondemand() result {}".format(result))
        except Exception as e:
            l.info("Antel::send_ondemand() result {}".format(e))
            result = None
        # return Antel._analizador_de_respuesta(result)
        return True

    @staticmethod
    def _analizador_de_respuesta(respuesta=None):
        return False if respuesta is None else respuesta.status_code == 200
