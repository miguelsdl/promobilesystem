import logging
l = logging.getLogger(__name__)


class Movistar:
    @staticmethod
    def send_ondemand(mensaje, comando):
        l.info("Movistar::send_ondemand()", [mensaje, comando])
        return True
