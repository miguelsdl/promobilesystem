import logging
from datetime import timedelta

from background_task import background
from django.conf import settings

from inapp.models.in_messages import InMessages

l = logging.getLogger(__name__)


class SaveIntoInMessages(object):

    @staticmethod
    @background(schedule=timedelta(seconds=settings.DEFAULT_DELAY_TO_IN_IN_MESSAGES_TABLE))
    def run(d):
        l.info("SaveIntoInMessages.run(**kwargs): {}".format(d))
        o = InMessages(**d)
        o.save()
