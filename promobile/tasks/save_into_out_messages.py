import logging

from send_diamond.models.out_messages import OutMessages

l = logging.getLogger(__name__)


class SaveIntoOutMessages(object):

    @staticmethod
    def run(command=None, message=None, request=None) -> bool:
        l.info("SaveIntoOutMessages.run(**kwargs): {} {}".format(command, message))
        try:
            # TODO - Arreglar esto, tiene que quedar uno solo
            o = OutMessages(
                oper=request.oper,
                command=-1 if command is None else command.id,
                message=message.message,
                msisdn=request.msisdn
            )
        except Exception as e:
            o = OutMessages(
                oper=request.get("oper"),
                command=-1 if command is None else command.id,
                message=message.message,
                msisdn=request.get("msisdn")
            )
            print(">X>>>>>>>>>>>>>>", str(e))
        o.save()
        return o.id


