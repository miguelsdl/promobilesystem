from wagtail.contrib.modeladmin.options import ModelAdminGroup, modeladmin_register

import inapp
import send_diamond
import voting.models


class VotacionesGroup(ModelAdminGroup):
    menu_label = "Votación"
    items = (
        voting.models.votings.Admin,
        voting.models.voting_elems .Admin,
        voting.models.voting_messages.Admin,
        voting.models.commands.Admin,
        inapp.models.in_messages.Admin,
        send_diamond.models.out_messages.Admin,
        inapp.models.short_code_messages.Admin,
        inapp.models.lista_negra.Admin,
        inapp.models.operators_shortcodes_keywords.Admin,
    )


modeladmin_register(VotacionesGroup)
