from voting.models.commands import Commands


class CommandType:

    def __init__(self, comando: Commands) -> dict:
        r = {
            "id": comando.id,
            "oper": comando.oper,
            "short_code": comando.short_code,
            "command": comando.command,
            "voting_id": int(comando.voting_id),
            "voting_elem_id": int(comando.voting_elem_id)
        }

        return r

    def __init__(self, lista_comandos: list) -> list:
        r = list()
        for comando in lista_comandos:
            d = {
                "id": comando.id,
                "oper": comando.oper,
                "short_code": comando.short_code,
                "command": comando.command,
                "voting_id": int(comando.voting_id),
                "voting_elem_id": int(comando.voting_elem_id)
            }
            r.append(d)

        return r
