
from promobile.lib.types.base_info import BaseInfo


class PromobileMessage(BaseInfo):

    def __init__(self, message: str = ''):
        super(PromobileMessage, self).__init__()
        self.__raw_message: str = message

        self.__process()

    def __process(self):
        message_parts = self.__raw_message.split(" ")
        self.command = message_parts.pop(0) if len(message_parts) else None
        self.second_command = message_parts.pop(0) if len(message_parts) else None
        self.tail = " ".join(message_parts)

