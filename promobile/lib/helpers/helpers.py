from inapp.models.short_code_messages import ShortCodeMessages
from voting.models.voting_messages import VotingMessages

messages_manager = {"short_code_messages": ShortCodeMessages, "voting_messages": VotingMessages}
