from django.db import models
from voting.models.commands import Commands

'''
Cenraliza la definición de loa campos de los modelos
'''


class FieldDefinition:

    oper = models.CharField(
        choices=[("1", "antel"), ("2", "movistar"), ("3", "claro"), ],
        max_length=1,
        verbose_name=u'Operadora',
    )

    short_code = models.CharField(max_length=8, verbose_name=u'Short code')
    command = models.CharField(max_length=64, verbose_name=u'Command')
    command_fk = models.ForeignKey(
        Commands,
        on_delete=models.CASCADE,
        verbose_name="Comando", null=True, blank=True

    )

    msisdn = models.CharField(max_length=64, verbose_name=u'msisdn')
    message = models.CharField(max_length=255, verbose_name=u'Mensage',)
    message_type = models.CharField(
        choices=[("1", "ERROR_COMANDO", ), ("2", "VOTACION_VENCIDA", ), ("3", "CONFIRMACION_VOTO", ), ],
        max_length=1,
        verbose_name=u'Tipo de mansaje',
        default=1
    )


class Keys:
    ERROR_COMANDO = '1'
    VOTACION_VENCIDA = '2'
    CONFIRMACION_VOTO = '3'
