# Generated by Django 3.0.6 on 2020-06-02 07:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('voting', '0004_auto_20200602_0710'),
    ]

    operations = [
        migrations.AddField(
            model_name='commands',
            name='billing_short_code',
            field=models.CharField(blank=True, default=None, max_length=8, null=True, verbose_name='Marcacion de cobro'),
        ),
        migrations.AlterField(
            model_name='votingmessages',
            name='voting',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='voting.Commands', verbose_name='Voting'),
        ),
    ]
