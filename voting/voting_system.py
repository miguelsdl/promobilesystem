import re

from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache

from inapp.lib.system_interface import SystemInterface

from promobile.lib.types.voting_message_type import VotingMessageType

from voting.models.commands import Commands
from voting.models.votings import Voting
from voting.models.voting_elems import VotingElems
from voting.models.voting_messages import VotingMessages
from send_diamond.diamonds_controller import DiamondsController
from send_diamond.models.out_messages import OutMessages
from promobile.tasks.save_into_out_messages import SaveIntoOutMessages
from promobile.lib.helpers.field_definition import Keys


import logging
l = logging.getLogger(__name__)


class VotingSystem(SystemInterface):

    def __init__(self, *args, **kwargs):
        super(VotingSystem, self).__init__()
        l.info("VotingSystem::VotingSystem()")

        self._voto = None

    def run(self) -> object:
        """
        VotingSystem::run() : Request: oper 1  sc 5555 mdg paco dn1234
        :return: bool
        """

        l.info("VotingSystem::run() : {}".format(str(self.get_data())))

        # TODO - acá puede dar un error sino crea todos los objetos
        self._voto = self._build_objects()

        voting = self._voto.get("my_voting", None)
        voting_elem = self._voto.get('my_voting_elem', None)
        command = self._voto.get('my_command', None)

        if voting is not None:
            # Si obtuve una votación
            if not voting.ended:
                # Si la votaciovon no venció
                self._add_one(voting_elem)
                msgs = self._get_next_user_message(command, message_type=Keys.CONFIRMACION_VOTO)
            else:
                # Si la votacion venció
                l.info("VotingSystem::run() votación vencida : {}".format(str(self._voto)))
                msgs = self._get_next_user_message(command=command, voting=voting, message_type=Keys.VOTACION_VENCIDA)
        else:
            # TODO - trvisar bien porque el comando incorrecto lo agarra antes
            # El comando era incorrecto para la votación, quiso votar algo que no es de la votacion
            l.info("VotingSystem::run() comando incorrecto : {}".format(str(self._voto)))
            msgs = self._get_next_user_message(command=command, voting=voting, message_type=Keys.ERROR_COMANDO)

        for msg in msgs:
            if self._add_message_to_send(msg, command):
                l.info("VotingSystem::run() mensaje agendado : {}".format(str(msg)))
            else:
                l.info("VotingSystem::run() mensaje no agendado: {}".format(str(msg)))

        return "Datos: {} Mensajes: {}".format(self._data, [m.message for m in msgs])

    def _add_one(self, my_voting_elem):
        o = self._voto.get("my_voting_elem")
        o.count += 1
        o.save()

    def _get_next_user_message(self, command=None, voting=None, message_type=None):
        """
        Este metodo carga todos los mensajes que se tienen que enviar al usuario con o sin cobro,
        la cantidad de mensajes depende de la operadora, trivia y demás, es algo particular, va a
        usar un módulo por operadora.
        :return: list
        """
        l.info("VotingSystem::_get_next_user_message() : {}".format(str(self.get_data())))
        if any((command, voting, message_type)):
            r = VotingMessages.objects.all()
            if command:
                r = r.filter(command_fk=command)
            if message_type:
                r = r.filter(message_type=message_type)
        else:
            r = list()

        return r

    def _add_message_to_send(self, message=("testing send messages", ), command=None):
        """
        Este metodo se encarga de hablar con el sistema de envío (a demanda) para dejar los mensajes
        a enviar, no le importa el tipo de mensaje, devuelve True si pudo insertar el mensaje sino False
        :return: Bool
        """
        l.info("VotingSystem::_add_message_to_send() : {}".format(str(message)))
        out_message_id = SaveIntoOutMessages.run(message=message, command=command, request=self.get_data())
        DiamondsController.send(**{"oper": self.get_data().oper, "message_id": message.id,
                                   "command_id": command.id, "request": self.get_data().__dict__(),
                                   "out_message_id": out_message_id, "message_manager_name": "voting_messages"})

        return True

    def _build_objects(self):
        my_command, my_voting, my_voting_elem = None, None, None
        my_command = self._build_command()
        if my_command is not None:
            my_voting = self._build_voting(my_command)
            if my_voting is not None:
                my_voting_elem = self._build_voting_elem(my_command)
            else:
                my_voting_elem = None

        return {"my_voting_elem": my_voting_elem, "my_voting": my_voting, "my_command": my_command}

    def _build_command(self):
        _data = self.get_data()
        r = Commands.objects.all().filter(oper=_data.oper, short_code=_data.short_code, command=_data.message)

        if len(r) > 0:
            l.info("VotingSystem::_build_command() OK {}".format(_data))
            # Me guardo el comando como variable de clase porque los voy a usar luego
            self._my_command = r[0]
        else:
            l.info("VotingSystem::_build_command() NOT FOUND {}".format(_data))

        return r[0]

    @classmethod
    def _build_voting(cls, command: Commands) -> Voting:
        try:
            voting = Voting.objects.get(pk=command.voting_id)
            l.info("VotingSystem::_build_voting() OK {}".format(vars(voting)))
        except ObjectDoesNotExist as e:
            # esto es comando incorrecto
            voting = None
            l.info("VotingSystem::_build_voting() NOT FOUND BY COMMAND: {}".format(command))

        return voting

    @classmethod
    def _build_voting_elem(cls, command: Commands) -> Voting:

        try:
            voting_elem = VotingElems.objects.get(pk=command.voting_elem_id)
            l.info("VotingSystem::_build_voting_elem() OK {}".format("command"))
        except ObjectDoesNotExist as e:
            voting_elem = None
            l.info("VotingSystem::_build_voting_elem() NOT FOUND {}".format("command"))

        return voting_elem

