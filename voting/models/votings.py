from django.db import models

from wagtail.snippets.models import register_snippet
from wagtail.snippets.blocks import SnippetChooserBlock
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import (FieldPanel, FieldRowPanel, MultiFieldPanel, StreamFieldPanel,)
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

class Voting(models.Model):

    name = models.CharField(
        max_length=128,
        verbose_name=u'Voting name',
    )

    start = models.DateTimeField(
        verbose_name=u'Voting start',
    )

    end = models.DateTimeField(
        verbose_name=u'Voting end',
        # auto_now=True
    )

    ended = models.BooleanField(
        default=False,
        verbose_name=u'Voting ended',
    )

    messages = StreamField(
        [("Click_para_agregar_un_mensaje", SnippetChooserBlock('voting.VotingMessages'))],
        default=None,
        null=True,
        blank=True,
        verbose_name=u'Mensajes'
    )

    def __str__(self):
        return self.name


    panels = [

        FieldPanel('name'),
        FieldPanel('start'),
        FieldPanel('end'),
        FieldPanel('ended'),
        StreamFieldPanel('messages'),
    ]


class Admin(ModelAdmin):
    model = Voting
    menu_label = "Votaciones"
    list_display = ('name', "start", "end", "ended", "messages")
    search_fields = ('command', "message")

