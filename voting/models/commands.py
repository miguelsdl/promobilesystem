from django.db import models

from inapp.lib.models.base import Base
from voting.models.voting_elems import Voting
from voting.models.voting_elems import VotingElems

from wagtail.snippets.models import register_snippet

from voting.models.votings import Voting

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register


class Commands(Base):

    voting = models.ForeignKey(
        Voting,
        on_delete=models.CASCADE,
        verbose_name="Voting"
    )

    voting_elem = models.ForeignKey(
        VotingElems,
        on_delete=models.CASCADE,
        verbose_name="Voting elem"
    )

    billing_short_code = models.CharField(max_length=8, verbose_name=u'Marcacion de cobro', null=True, blank=True, default=None)

    def __str__(self):
        return self.command


class Admin(ModelAdmin):
    model = Commands
    menu_label = "Comando votacion"
    list_display = ('oper', "short_code", 'command', "voting", "voting_elem")
    search_fields = ('oper', "short_code", 'command', "voting", "voting_elem")

