from django.db import models

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from voting.models.votings import Voting


class VotingElems(models.Model):

    name = models.CharField(
        max_length=128,
        verbose_name=u'Voting name',
    )

    voting_id = models.ForeignKey(
        Voting,
        on_delete=models.CASCADE,
        verbose_name="Voting"
    )

    name = models.CharField(
        max_length=128,
        verbose_name=u'Name of element',
    )

    count = models.IntegerField(
        default=0,
        verbose_name=u'Voting element counter',
    )

    def sum_1(self) -> None:
        self.count += 1
        self.save()

    def __str__(self):
        return self.name

    @classmethod
    def get_statistics_by_voting_id(cls, voting_id: int) -> dict:
        pass


class Admin(ModelAdmin):
    model = VotingElems
    menu_label = "Votados"
    list_display = ('name', "voting_id", 'name', "count")
    list_filter = ('name', "voting_id", 'name', "count")
    ordening = ('name', "voting_id", 'name', "count")
    search_fields = ('name', "voting_id", 'name', "count")

