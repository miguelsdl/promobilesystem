from django.db import models

from promobile.lib.helpers.field_definition import FieldDefinition
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register


class VotingMessages(models.Model):

    command_fk = FieldDefinition.command_fk
    message = FieldDefinition.message
    message_type = FieldDefinition.message_type

    @property
    def slug(self):
        return "Mensaje-votacion"

    def __str__(self):
        return self.message


class Admin(ModelAdmin):
    model = VotingMessages
    menu_label = "Mesanjes"
    list_display = ('command_fk', "message", "message_type")
    list_filter = ('command_fk', "message", "message_type")
    ordening = ('command_fk', "message", "message_type")
    search_fields = ('command_fk', "message", "message_type")

