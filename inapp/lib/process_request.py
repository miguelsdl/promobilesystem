from django.conf import settings

from inapp.lib.types.request import Request
from inapp.models.in_messages import InMessages
from promobile.tasks.save_into_in_messages import SaveIntoInMessages
'''
    Encapsula todos los datos de una reuqest y las funcionalidades internas
'''


class ProcessRequest:
    _oper = None
    _short_code = None
    _message = None
    _msisdn = None

    def __init__(self, request,  *args, **kwargs):
        """
        Son las cuatro variables de una request que se procesa, vienen todas en kwargs
        :param args:
        :param kwargs:
        """
        var = request.GET
        self._oper = var.get('oper', None)
        self._short_code = var.get('short_code', None)
        self._message = var.get('message', None)
        self._msisdn = var.get('msisdn', None)
        self._tr_id = var.get('tr_id', None)

    def proc(self):
        """
        procesa los datos y los devuelve
        :return:
        """
        self._check_request()
        vals = {"oper": self._oper, "short_code": self._short_code, "message": self._message, "msisdn": self._msisdn}
        SaveIntoInMessages.run(vals)
        vals.update({"tr_id": self._tr_id})
        return Request(**vals)

    def _check_request(self):
        vals = {"oper": self._oper, "short_code": self._short_code, "message": self._message, "msisdn": self._msisdn}
        for k, v in vals.items():
            if v is None:
                raise Exception("Error en request inicial el valor de  {k} es {v}".format(k=k, v=v))


