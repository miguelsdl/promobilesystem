from django.db import models


class Base(models.Model):
    oper = models.CharField(
        choices=[("1", "antel"), ("2", "movistar"), ("3", "claro"), ],
        max_length=1,
        verbose_name=u'Operadora',
    )

    short_code = models.CharField(max_length=8, verbose_name=u'short_code')
    command = models.CharField(max_length=64, verbose_name=u'command')

    class Meta:
        abstract = True
