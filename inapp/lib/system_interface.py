from inapp.lib.types.request import Request as RequestType


class SystemInterface(object):

    def __init__(self):
        self._data = None

    def get_data(self):
        return self._data

    def set_data(self, data: RequestType) -> bool:
        try:
            self._data = data
        except Exception as e:
            print(e)
            return False
        return True

    def run(self) -> bool:
        raise NotImplementedError("This method should be implemented")

