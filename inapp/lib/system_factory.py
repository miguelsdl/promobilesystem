from inapp.models.operators_shortcodes_keywords import OperatorsShortcodesKeywords
from inapp.lib.types.request import Request as RequestType
from send_diamond.diamonds_controller import DiamondsController
from voting.voting_system import VotingSystem
from inapp.inapp_system import InappSystem

class SystemFactory:

    @staticmethod
    def get_system(request_processed: RequestType) -> object:
        system_id = OperatorsShortcodesKeywords.get_system(request_processed)

        if int(system_id) == 10:
            return VotingSystem()

        else:
            return InappSystem(request=request_processed)




