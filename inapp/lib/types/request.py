class Request:

    def __init__(self, **kwargs):
        self.oper = int(kwargs.get('oper', None))
        self.short_code = kwargs.get('short_code', None)
        self.message = kwargs.get('message', None)
        self.msisdn = kwargs.get('msisdn', None)
        self.tr_id = kwargs.get('tr_id', None)

    def __str__(self):
        return "Request: oper {}  sc {} mdg {} dn {} tr_id {}".format(self.oper, self.short_code, self.message, self.msisdn, self.tr_id)

    def __dict__(self):
        return {
            "oper": self.oper,
            "short_code":self.short_code,
            "message": self.message,
            "msisdn": self.msisdn,
            "tr_id" :self.tr_id,
        }
