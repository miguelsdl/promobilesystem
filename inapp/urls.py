from django.urls import path
from django.conf.urls import url
from inapp.views import process

from inapp.views.cdc_in import my_soap_application


urlpatterns = [
    path("inapp/", process, name='process'),
    path('soap_service/', my_soap_application, name="my_soap_application"),
]