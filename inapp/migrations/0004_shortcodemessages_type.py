# Generated by Django 2.2.12 on 2020-06-06 02:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inapp', '0003_shortcodemessages'),
    ]

    operations = [
        migrations.AddField(
            model_name='shortcodemessages',
            name='type',
            field=models.CharField(choices=[('1', 'ERROR_COMANDO')], default=1, max_length=1, verbose_name='Tipo de mansaje'),
        ),
    ]
