import logging

from django.db import models

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from inapp.lib.types.request import Request as RequestType
from promobile.lib.helpers.field_definition import FieldDefinition

l = logging.getLogger(__name__)


class OperatorsShortcodesKeywords(models.Model):

    oper = models.CharField(
        choices=[("1", "antel"), ("2", "movistar"), ("3", "claro"), ],
        max_length=1,
        verbose_name=u'Operadora',
    )

    short_code = models.CharField(max_length=8, verbose_name=u'short_code')
    command = FieldDefinition.command_fk
    system = models.CharField(
        choices=[("10", "votation"), ("20", "trivia"), ("30", "Suscripcion"), ],
        max_length=2,
        verbose_name=u'Systema',
    )

    class Meta:
        unique_together = (('oper', 'short_code', 'command_fk', 'system'),) # constraint

    @staticmethod
    def get_system(r: RequestType):
        result = None
        try:
            s = OperatorsShortcodesKeywords.objects.\
                filter(oper=r.oper).\
                filter(short_code=r.short_code).\
                filter(command_fk__command=r.message).first()
            result = s.system
        except Exception as e:
            l.info("OperatorsShortcodesKeywords::get_system() {} {} ".format(r, e))
            result = False

        return result


class Admin(ModelAdmin):
    model = OperatorsShortcodesKeywords
    menu_label = "Ops " \
                 "shortcodes" \
                 " kw"
    list_display = ('oper', "short_code", "command_fk", "system")
    list_filter = ('oper', "short_code", "command_fk", "system")
    ordening = ('oper', "short_code", "command_fk", "system")
    search_fields = ('oper', "short_code", "command_fk", "system")


