from django.db import models

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register


class ShortCodeMessages(models.Model):

    oper = models.CharField(
        choices=[("1", "antel"), ("2", "movistar"), ("3", "claro"), ],
        max_length=1,
        verbose_name=u'Operadora',
    )

    short_code = models.CharField(max_length=8, verbose_name=u'short_code')

    message = models.CharField(
        max_length=255,
        verbose_name=u'Mensage',
    )

    type = models.CharField(
        choices=[("1", "ERROR_COMANDO"), ],
        max_length=1,
        verbose_name=u'Tipo de mansaje',
        default=1
    )

    @property
    def slug(self):
        return "ShortCodeMessages"

    def __str__(self):
        return self.message


class Admin(ModelAdmin):
    model = ShortCodeMessages
    menu_label = "Short codes messages"
    list_display = ('oper',  'short_code', "message", "type")
    list_filter = ('oper',  'short_code', "message", "type")
    ordening = ('oper',  'short_code', "message", "type")
    search_fields = ('oper',  'short_code', "message", "type")

