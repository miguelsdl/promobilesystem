from django.db import models

from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from inapp.lib.models.base import Base


class InMessages(Base):
    message = models.CharField(max_length=64, verbose_name=u'mensaje')
    msisdn = models.CharField(max_length=64, verbose_name=u'msisdn')


class Admin(ModelAdmin):
    model = InMessages
    menu_label = "Mensajes entrantes"
    list_display = ('oper',  'msisdn', "command", "message")
    list_filter = ('oper',  'msisdn', "command", "message")
    ordening = ('oper',  'msisdn', "command", "message")
    search_fields = ('oper',  'msisdn', "command", "message")
