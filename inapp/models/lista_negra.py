from django.db import models

from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)


class BlackList(models.Model):

    oper = models.CharField(
        choices=[("1", "antel"), ("2", "movistar"), ("3", "claro"), ],
        max_length=1,
        verbose_name=u'Operadora',
    )

    msisdn = models.CharField(max_length=64, verbose_name=u'msisdn')

    @staticmethod
    def check_if(msisdn=None, oper=None,):
        result = None
        try:
            q = BlackList.objects.filter(oper=oper).filter(msisdn=msisdn)
            result = True if len(q) > 0 else False
        except Exception as e:
            print(str(e))
            result = False

        return result


class Admin(ModelAdmin):
    model = BlackList
    menu_label = 'Lista negra nrs.'
    list_display = ('oper',  'msisdn', )

