from django.http import HttpResponse
from inapp.lib.process_request import ProcessRequest
from inapp.models import BlackList
from inapp.lib.system_factory import SystemFactory
from voting.voting_system import VotingSystem
from send_diamond.diamonds_controller import DiamondsController

'''
Esta vista toma recibe las peticiones y llama a ProcessRequest(request) para procesarla

Verifica la lista negra

Sino está en la lista negra manda los datos al sismeta correspondientey da run

Devuelve una respuesta 
'''
from django.core.cache import cache


def process(request):
    proc_req = ProcessRequest(request)
    request_processed = proc_req.proc()
    in_black_list = BlackList.check_if(oper=request_processed.oper, msisdn=request_processed.msisdn)
    if not in_black_list:
        system = SystemFactory.get_system(request_processed)
        system.set_data(request_processed)
        o = system.run()

    else:
        pass
    return HttpResponse(o)


# http://127.0.0.1:8000/main/inapp/?oper=3&short_code=5555&msisdn=1234&message=vota paco
