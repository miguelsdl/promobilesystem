import logging
from django.conf import settings

from inapp.lib.system_interface import SystemInterface
from inapp.lib.types.request import Request as RequestType
from inapp.models.short_code_messages import ShortCodeMessages
from send_diamond.diamonds_controller import DiamondsController
from promobile.tasks.save_into_out_messages import SaveIntoOutMessages


l = logging.getLogger(__name__)


class InappSystem(SystemInterface):

    def __init__(self, request: RequestType):
        l.info("InappSystem::__init__():")
        super(InappSystem, self).__init__()
        self._request = request

    def run(self) -> bool:
        l.info("InappSystem::run():")
        self.incorrect_command_by_short_code()

    def incorrect_command_by_short_code(self):
        l.info("InappSystem::incorrect_command_by_short_code():")

        message = ShortCodeMessages.objects.all().\
            filter(oper=self._request.oper).\
            filter(short_code=self._request.short_code).\
            filter(type=settings.ERROR_COMANDO).first()

        if message:
            out_message_id = SaveIntoOutMessages.run(None, message, self._request)
            DiamondsController.send(
                **{"oper": self.get_data().oper, "message_id": message.id,
                   "request": self.get_data().__dict__(),
                   "out_message_id": out_message_id, "message_manager_name": "short_code_messages"}
            )
